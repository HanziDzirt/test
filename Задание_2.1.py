#Плюсы: простота реализации и высокая читаемость за счёт сохранения порядка элементов в плоской структуре 
#Минус: алгоритмическая сложность операции добавления при таком решении о(n)

class FIFO_append:
    data = None
    
    def give_data(self, data):
        self.data = data
    
    def print_data(self):
        print(self.data)
    
    def add(self, x):
        sekf.data.pop(0)
        self.data.append(x)
        
FIFO = FIFO_append()
file = [1, 2, 3, 4, 5]
FIFO.give_data(file)
FIFO.print_data()
FIFO.add(6)
FIFO.print_data()
FIFO.add(7)
FIFO.print_data()
FIFO.add(8)
FIFO.print_data()
