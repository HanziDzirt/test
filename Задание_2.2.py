#Плюсы: алгоритмическая сложность операции добавления при таком решении о(1)
#Минус: Плохая читаемость, нужно смотреть индекс

class FIFO_shift:
    data = None
    index = 0
    size = None
    
    def give_data(self, data, index=0, size=0):
        size=len(data)
        self.data = data
        self.index = index
        self.size = size
    
    def print_data(self):
        print(self.data)
        print(self.index)
        print(self.size)
    
    def add(self, x):
        self.data[self.index] = x
        self.move_index(self)
    
    def move_index(self, index):
        if self.index<self.size-1:
            self.index=self.index+1
        else:
            self.index=0
    
FIFO = FIFO_shift()
file = [1, 2, 3, 4, 5]
FIFO.give_data(file)
FIFO.print_data()
FIFO.add(6)
FIFO.print_data()
FIFO.add(7)
FIFO.print_data()
FIFO.add(8)
FIFO.print_data()
